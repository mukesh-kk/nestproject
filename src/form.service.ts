import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Form } from "./form.model";

@Injectable()
export class FormServices {
  constructor(@InjectModel("Form") private readonly userModel: Model<Form>) {}

  async findAll(): Promise<Form[]> {
    return this.userModel.find().exec();
  }

  async findOne(id: string): Promise<Form> {
    return this.userModel.findById(id).exec();
  }

  async create(form: Form): Promise<Form> {
    const newUser = new this.userModel(form);
    return newUser.save();
  }

  async update(id: string, form: Form): Promise<Form> {
    return this.userModel.findByIdAndUpdate(id, form, { new: true }).exec();
  }

  async delete(id: string): Promise<Form> {
    return this.userModel.findByIdAndDelete(id).exec();
  }
}
