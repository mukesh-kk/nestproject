import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from "@nestjs/common";
import { Form } from "./form.model";
import { FormServices } from "./form.service";

@Controller("forms")
export class FormController {
  constructor(private readonly formServices: FormServices) {}

  @Get()
  async findAll(): Promise<Form[]> {
    return this.formServices.findAll();
  }

  @Get(":id")
  async findOne(@Param("id") id: string): Promise<Form> {
    return this.formServices.findOne(id);
  }

  @Post()
  async create(@Body() form: Form): Promise<Form> {
    console.log(form, "form -12");
    return this.formServices.create(form);
  }

  @Put(":id")
  async update(@Param("id") id: string, @Body() form: Form): Promise<Form> {
    return this.formServices.update(id, form);
  }

  @Delete(":id")
  async delete(@Param("id") id: string): Promise<Form> {
    return this.formServices.delete(id);
  }
}
