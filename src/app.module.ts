import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { MongooseModule } from "@nestjs/mongoose";
import { FormModule } from "./fomr.module";
@Module({
  imports: [
    MongooseModule.forRoot(
      "mongodb+srv://mukesh-kkkk:123456789QWERT@cluster0.1s7qq.mongodb.net/?retryWrites=true&w=majority"
    ),
    FormModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
