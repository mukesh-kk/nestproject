import * as mongoose from "mongoose";

export const FormSchema = new mongoose.Schema({
  title: String,
  fields: [mongoose.Schema.Types.Mixed],
  created_at: {
    type: Date,
    default: Date.now(),
  },
});

export interface Form extends mongoose.Document {
  id: string;
  title: string;
  fields: any[];
  created_at: Date;
}
