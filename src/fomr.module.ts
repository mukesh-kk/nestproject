import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { FormSchema } from "./form.model";
import { FormController } from "./form.controller";
import { FormServices } from "./form.service";

@Module({
  imports: [MongooseModule.forFeature([{ name: "Form", schema: FormSchema }])],
  controllers: [FormController],
  providers: [FormServices],
})
export class FormModule {}
