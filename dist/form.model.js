"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FormSchema = void 0;
const mongoose = require("mongoose");
exports.FormSchema = new mongoose.Schema({
    title: String,
    fields: [mongoose.Schema.Types.Mixed],
    created_at: {
        type: Date,
        default: Date.now(),
    },
});
//# sourceMappingURL=form.model.js.map