import * as mongoose from "mongoose";
export declare const FormSchema: mongoose.Schema<any, mongoose.Model<any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, {
    fields: any[];
    created_at: Date;
    title?: string;
}>;
export interface Form extends mongoose.Document {
    id: string;
    title: string;
    fields: any[];
    created_at: Date;
}
