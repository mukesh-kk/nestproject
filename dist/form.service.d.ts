import { Model } from "mongoose";
import { Form } from "./form.model";
export declare class FormServices {
    private readonly userModel;
    constructor(userModel: Model<Form>);
    findAll(): Promise<Form[]>;
    findOne(id: string): Promise<Form>;
    create(form: Form): Promise<Form>;
    update(id: string, form: Form): Promise<Form>;
    delete(id: string): Promise<Form>;
}
