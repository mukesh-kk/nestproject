import { Form } from "./form.model";
import { FormServices } from "./form.service";
export declare class FormController {
    private readonly formServices;
    constructor(formServices: FormServices);
    findAll(): Promise<Form[]>;
    findOne(id: string): Promise<Form>;
    create(form: Form): Promise<Form>;
    update(id: string, form: Form): Promise<Form>;
    delete(id: string): Promise<Form>;
}
